---
title: Notes8/9 - 11/13 - JasperGold tutorials - Assertions 
date: 2023-11-13 14:00:00 -0400
pin : true
---
This post will detail my research for JasperGold Assertions.
*These notes were submitted on 2/7/24*
# Comparing JasperGold to Formality and Conformal (11/13)
- Conformal and Formality's Usage
    - They are meant to be used only for Logical Equivalence Checking. 
    - Meant for checking RTL of a verified model and to compare it against a working model
    - Has nothing to do with assertions or other equivalence checking
    - For user wanting to use other Equivalence checking or go to the netlist level, this isn't the tool for it

- Jaspergold
    - A suit of tools that includes Formal verification, Sequential verification and many more
    - Can also do logical equivalence
    - Has plenty of docuentation and use cases in industry
    - Allows simple insertions of covers, SVA, assumptions and more
    - For JasperGold Tutorials, it involves learning more about SVA and formal verification as a whole

- Conclusion
    - Do to these factors, it has been concluded that Jaspergold will be the main tool for processor and part research for the remainder of the fellowship
    
# Assertions Tutorials and research (11/15)
- *Over these few weeks I have done tutorials on Assertions so I can properly use them in JasperGold. Below will containing some snipits of the notes I learned and what can be useful for people to use in the future to start to undertand Assertions*
    - The Fundamental Reseason for any Verification Methodology
        - To have 100% coverage and to ensure there are no bugs.
        - Used for SVA, Verilog, and other HDL languages
    - Simple Terminology
        - Coverage
            - Checking end to end of a given code a user wants coverage of
            - To ensure thorough verification
        - Assertions
            - Embedded checks in code
            - If the check, or assertion, fails, then there is likely an issue in the code to not pass the specific case
    - Assertion Specifics
        - They're 3 types of behaviors for assertions
            - Expected Behavior
                -The Assertion and hdl passes and does what was specified
            - Forbidden Behavior
                - Passes a check that isn't suppose to pass
            - Signal Protocols
                - Protocols that test the bus's correct behavior
        - Types of Assertions
            - Abstract
                - Assertions to meet functional specifications
            - Functional
                - Assertions check properties that the RTL designer intends to have
            - Functional Coverage
                - Assertions for corner cases
            - IP Assertions
                - Assertions for checking the IP user and the other end IP is being used correctly
    - Assertion Syntax
        - Types
            - Invariant
                - Assertions that always stay the same
                - EX: A and B will never go high period
            - Temporal
                - Assertions evaluated over time
                - EX: A is high on one cycle, then B high the next, and then C after
            - Immediate
                - When hit in the code it is immediately used
                - Similar to an if statement in C
                - If A then B must happen
            - Concurrent
                - Used to describe a general behavior attribute in a design
                - Evaluates assertions in isolation
        - Assertions vs Assumptions
            - Property
                - Logical and Temporal Relationship between boolean and sequential
            - Assertions
                - Do not confuse Assertions with properties
                - A verification directive
                - EX: Tell a EDA tool what *to do* with a property
            - Assumptions
                - In **formal**, it is to allow the DUT and inputs to behave only in a way that never violates a property 
        - Other Important terms
            - Cover
                - In **formal**, it shows something happening in a range selected and that the expression is true
            - Restrict
                - In **formal**, it the same thing as assumptions
    - Assertion Usage
        - Naming Assertions
            - Declare a property
            - Then instantiate it
            - Example:
                - Declaration:
                    - property **PROP1**;
                        (**EN1** | **EN2**);
                        endproperty
                -Instantiation:
                    - **ASSERT1**: assert propety (**PROP1**);
        - Clks
            - *Note: If there is no clk in auxilary code or helper code a user needs to sepcify a default clk or say none at all*
            - Example:
                - Declaration:
                    - default clocking **DEFCLK** @(posedge **CLK**);
                      endclocking
                - Used in code:
                    - @(posedge **CLK1**) (**EN1** | **EN2**);
                      endproperty
    - Assertion Placement
        - Can be Placed in:
            - Module
            - Inferface
            - Procedural code (Not reccomended)
        - try to always use *ifdefs* for assertions
    - Quick Tips
        - Same Cycle Implication:
            - **|->**
        - Next Cycle Implication:
            - **|=>**
        - Assert
            - Tells the tool to execute the assertion
        


