---
title: Notes19/21 - 1/22 - Sequential Equivalence in Jasper Gold
date: 2024-01-22 14:00:00 -0400
pin : true
---
This post will detail my research on using Sequential Equivalence in JasperGold. I have prior notes from the labs and placing processors into the normal formal tool, but for now, this is the one most prominent with current research
*These notes were submitted on 2/8/24*
# Sequential Equivalence Checking in JasperGold (01/22, 01/24, 01/31)
 - Sequential Equivlalence Usage
    - Allows for equivalence checking but doesnt depend on timing
    - Works great for processors of different complexities to ensure the functionalites are the same
    - Testing 2 ALU files for Sequential Equivalence
- Findings
    - Unlike the broad Formal tutorial, little notation on Sequential Equivalence in Jasper
    - Formality and Confluence worse comparitibely to compare two different ALU's, as they are mainly just for exact spec and implementation
    - To get to Sequential Equivalence Checking
        - View > Application >Sequential Equivalence checking
    - When TCL file is officiated, then it is smooth sailing like other Jasper Apps
- Notations and TCL file
    - ***Both of my TCL files work below, but tcl 1 just sets them up and 2 is an attempt to do the equivalence checking***
    - To do basic analysis of a file and if its a certain verilog type, do the following (both spec and imp):
        - check_sec -analyze -spec -v2k {/home/net/user/research/jaspergold/ALU8bit3.v} ;
    - TCL Example File
        - ``` 
            % check_sec -clear
            % check_sec -analyze -spec -sv -f modelA.vfile
            % check_sec -elaborate -spec modelA
            % check_sec -analyze -imp -sv -f modelB.vfile
            % check_sec -elaborate -imp -top modelB
            % check_sec -setup
            # change only the imp side
            % check_sec -setup -clear
            % check_sec -elaborate -imp -clear
            % check_sec -analyze -imp -sv -f modelA.vfile
            % check_sec -elaborate -imp -top modelA
            % check_sec -setup ```
    - TCL File 1
        - ```
            check_sec -analyze -spec -v2k {/home/net/la474256/research/jaspergold/ALU8bit3.v} ;
            check_sec -elaborate -spec -f ALU8bit3.v
            check_sec -analyze -imp -v2k {/home/net/la474256/research/jaspergold/ALU8bit.v} ;
            check_sec -elaborate -imp -f ALU8bit.v
            check_sec -setup
            reset -none
            clock -none
            check_sec -map -spec {ALU_Result} -imp {alu_imp.ALU_Result} -regexp
            prove -bg -all ```
    - TCL File 2
        - ```
            check_sec -analyze -spec -v2k {/home/net/la474256/research/jaspergold/ALU8bit3.v} ;
            check_sec -elaborate -spec -f ALU8bit3.v
            check_sec -analyze -imp -v2k {/home/net/la474256/research/jaspergold/ALU8bit.v} ;
            check_sec -elaborate -imp -f ALU8bit.v
            check_sec -setup -spec_dut {/home/net/la474256/research/jaspergold/ALU8bit_tesbench.v} ;
            check_sec -setup -imp_dut {/home/net/la474256/research/jaspergold/ALU8bit_tesbench.v} ;
            check_sec -setup
            reset -none
            clock -none
            prove -bg -all



            




            
