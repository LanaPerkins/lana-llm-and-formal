---
title: Notes4/5 - 10/30 - Formality Continued, Labs 1-3
date: 2023-10-30 14:00:00 -0400
pin : true
---
This post is about my notes on labs 1-3. Further Labs were experimented with, however, little notes were taken as they were more logical issues.
*These notes were submited on 11/14/23*

# Formality Lab Notes
- These labs were taken from the synopsys training videos and were integral in allowing me to learn the tool. The actual labs are on the synopsys website which needs prior access to.
- For the labs, there was also a guide document that explained the process of each one.
- Formality Lab 1
    - This lab was about just following the formality guide on starting formality.
    - To make the script run this command
        - Fm_mk_script
    - To run like a make file, or all the commands that needs to be run in one command
        - run the .svf file
    - To exit formality
        - press q to quit and press enter
- Formality Lab 2
    - This lab explained accessing the gui to verify the design
    - Restate what formality does
        - It can have a golden, or proven verilog code, and then compare it with a new verilog code for logical equivalence
    - To open the gui
        - Formality
        - fm_shell -gui
    - Generally click auto setup upon gui startup for smoother experience
    - Most others thing indicated in the tutorial has already been experiented with in prior notes
- Formality Lab 3
    - The goal of this lab is to compare 2 gates, a and and or gate and see how formality verifies them
    - Found out that its really done on a schematic level
    - If they do not thave the same top design then there willl be issues comparing