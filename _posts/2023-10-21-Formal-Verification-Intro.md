---
title: Notes1 - 10/21-10/23 - Intro to Formal Verification
date: 2023-10-21 12:00:00 -0400
pin : true
image:
  path: /assets/images/Formal-Verification-Reviews2.png   
---
This post is the start on the journey for Formal Verification. It is the research of Formal Verification, and a subset of that study, Equivalence Checking.
*These notes were submited on 11/9/23*
# Formal Verification
- Definition 
    - This is the practice of verifying a systems properties using mathematical properties
    - This has been done using:
        - vcs 
        - Verdi
        - Synopsys
        - Formality
        - exc.
- Why this is important
    - Simulation verification doesn't check for every possible outcome
        - A user simulates a design by putting it into a simulator and it tests random values
        - It is exhaustive and can lead to exponential time
    - Formal Verification just needs a correct mathematical proof to confirm a design works
        - Requires extensive knowledge of the design
        - Takes a shorter amount of time, and only needs to do 1 test case to test every test case
        
- Formal Verification in Practice, VIPER's ALU
    - https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=713c3bd236d8c02e33a9046dd6be153bd138ddf0
    - Summary
        - They used there own specilized formal method to formally verify this ALU
        - It is different, but it is still finding a mathematical proof, and verifying that the design works as intended
    - Viper ALU
        - Input 2 32 bit Operands and returns a result
        - Performs 13 operations
    - Report Findings
        - Under the HOL formal verification, the method was a success and found this ALU to not pass formal verification
        
- Formal Verification in Practice 2, ARM Procesor
    - https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=745161
    - Summary
        - This report details the combination of a ARM7 and Strong ARM processor
        - Studys if the combination is viable through Formal Verification methods.
    - The ALU
        - Uses pipeline from Strongarm, uses memory from ARM7
        - It has a 5 - stage instruct pipeline
        - Predicated Execution
        - Has Forwarding Logic
        - 32 bit data and address bus
        - ARM CPU Core
        - RISC processor Macrocell
    - Report Methodology
        - Used abstract formal assertions to study the ALU combination
        - Symbolic Trajectory was used
            - The main formal verification method
            - Verificied the set of trajectory asserstion on the implementation
        - Defined instruction set as set of abstract assertions
    - Conclusion
        - Found 4 bugs, lead to further understanding of formal and the processors.
# Equivalence Checking
- What is Equivalency Checking/ Logical Equivalence?
    - https://www.synopsys.com/glossary/what-is-equivalence-checking.html#:~:text=Equivalence%20checking%20is%20a%20portion%20of%20a%20larger,simulation%20to%20verify%20the%20correctness%20of%20a%20design.
    - Definition
        - Using Formal verification between a verified and a nonverified product to ensure both are functionally the same
    - Methodology
        - Setup
            - Set the inputs, reference design (verified), and new design (unverified)
        - Mapping
            - Map the inputs of both designs to eachother
            - Pretty much make sure that both design have similar inputs and outputs so even if the inside machine is different, its simple to set up for simulating results
        - Compare
            - Determine if the Outputs for given stimuli (inputs) lead to same answers
    - How is it Formal?
        - Uses the vectorless formal proof approach
    - Logical vs Sequence Equivalence Checking
        - Logical
            - Looks at the combinational structure of the circuit
        - Sequence
            - Looks at combination structure and accounts timing into the equation
- Equivalency Checking Application, VC Formal
    - https://www.synopsys.com/verification/static-and-formal-verification/vc-formal.html#seq
    - Transactional Equivalence
        - Compares C/C++ models in RTL for equivalence.
        - Synopsys VC formal DPV
        - Good for comparing C++ and RTL models
    - Sequential Equivalence
        - Checks 2 RTL designs to produce same output every clk cycle
        - Synopsys VC formal SEQ
        - Doesn’t require user assertions 
# Nonspecified Notes
- Carry-lookahead vs Ripple Carry Adder
    - Carry-lookahead
        - Harder and clever but propogates faster than ripple carry
        - More logic
    - Ripple Carry
        - Less logic but uses chain of half and full adders
- Instruction Sets
    - The base architecture of how the processor does logical and mathematical operations
    - Names Used:
        - RISC, MISC, exc.