---
title: Notes6/7 - 11/3 - Formality Custom ALU practice
date: 2023-11-3 14:00:00 -0400
pin : true
---
This post is about my notes on custom ALU's using the Formality. This allowed me to begin the process of developing my own ALU's and how custom ones work in the tool.
*These notes were submited on 11/14/23*
# Formality ALU Test
- ALU's That I tested
    - https://www.fpga4student.com/2017/06/Verilog-code-for-ALU.html
    - https://edaplayground.com/x/nvgq
- Info on the ALU's
    - The First ALU
        - 8 bit 2 input ALU
        - 8 bit result
        - Carry out flag for the 9th bit carry
        - 4 bit operator selecting flag
        - Worked based off of case statements
        - Defaults to just adding both inputs
    - The Second ALU
        - 8 bit 2 input ALU
        - 9 bit result
        - No carry flag
        - 4 bit operator flag
        - Has Multiplication already commented out
            - Will come back to this later
        - Case statements for operations
        - Default is 0
- Operating the ALU's
    - Upon first comparing them
        - Both are equivalent besides the default statement
        - Changed the default statement to compensate
            - Change default to both to zero
    - Upon Running in Formality
        - Both were not equivalent
        - When both were compared in schematics
            - Showed that both ALU's had the same top (testbench)
            - However, the exact mapping wasn't the same
    - Issue
        - The commented out multiplication caused issues and when that 4'b0010 ran
            - ALU 1 had values for multiplication
            - ALU 2 had no values for multiplication, forever staying at 0 do to default
    - Fix
        - Uncommented the multiplication
            - Made them both logically equivalent
# What I Learned and Future Plans
- Learned that sometimes, looking at the code may be quicker than trying to debug in the tool
- Formality was limited to only Logical Equivalence, Will study Jaspergold to find more usage for Logical Equivalence and Formal Verification
- Will look more into ALU creation at a later date
- Learned how to use formality and further understanding into ALUs