---
title: Notes2 - 10/26 - Tool to Study
date: 2023-10-26 14:00:00 -0400
pin : true
---
This post is about deciding which tool to study for the next week. This post will be discussing JasperGold and Formality
*These notes were submited on 11/9/23*
# Formal Verification Tools
- There are multiple Tools to do Formal Verification with. There were 2 tools that were promising:
    - Formality
        - A formal tool based on equivalency checking schematic input and outputs
        - Made by Synopsys
    - Jaspergold
        - A Integrated Formal Verification Tool
        - Made by Cadence
# Formality Intro
- Tutorial Video
    - https://www.youtube.com/watch?v=CFRqPnqifx0
- Started this Tutorial to get adjusted to the software
- Began with testing a ALU with itself, to ensure proper procedure
    - https://github.com/Nidhinchandran47/4-bit-alu/tree/main/new
- Procedure
    - Reference
        - Place your reference, you verified product, into the Reference tab
        - Then place its top module (the instantiator)
            - Can be itself, but can be in its own .v instatiator file or the testbench file 
    - Implimentation
        - Place your testing module. It is the module that you want to see if its equivalent
        - Place the Top Module (the instantiator)
    - Match
        - Match all the intputs and outputs to eachother for the input values
        - if not the same names need to manually do it oneself
        - *to save yourself a headache just give them the same names in Reference and Implimentation*
    - Verify
        - This will see if they are a equivalent using the mathematical algorithm
    - Debug
        - If not equivalent, there is a debugger that can give info on the issue.
- Experience
    - I used a ALU and didn't place all the files nor specify the correct top design. The video used a ALU which I didn't have access too, so I just tried to read the manual to learn more about the info. The video did help guide me to see what the correct info would be.
        - http://thuime.cn/wiki/images/6/63/Formality_Lab-2018.06.pdf
    - Decided to persue more research on Formality for next research session.
# JasperGold Intro
- This is a Formal Verification Tool from Cadence
- Has more than just Equivalence Checking
- Usage Video
    - https://www.youtube.com/watch?v=0cEkWpE5nUM&list=WL&index=19
- Pros and Cons
    - Pros
        - More Usage Than Formality
        - Has uses for Verilog, c and c++ implementations
    - Cons
        - Complex, hard to navigate without a guide
        - Need access, no access as of  10/26/23
# Outcome and Further Plans
- I will start using Formality, and study what it has. 
- Will work on ALU research in later sessions