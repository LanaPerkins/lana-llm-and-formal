# Lana Perkins's Lab Notebook

Visit the live lab notebook [here](https://lperkins-ucfdracolab-notebooks-86051f3cb761cd40e87ff701b066ab74.gitlab.io/).

This is based on the most recent commit in the `master` branch. 

## Prerequisites

Follow the instructions in the [Jekyll Docs](https://jekyllrb.com/docs/installation/) to complete the installation of
the basic environment. [Git](https://git-scm.com/) also needs to be installed.

## Installation

First time - clone this directory to your local machine and in that director run:

```console
$ bundle
```

## Development

From the cloned directory, local test 

```console
$ bundle exec jekyll s
```

## Normal Usage:

Create new posts for each day in the `_posts` directory. See the existing posts for help on how to create and edit the markdown files.
At a minimum filenames for daily lab notebook enties must start with the date in this format (`YYYY-MM-DD`) and be followed by the sequentially numbered four-digit entry (starting at `E0001`), and finally ending with an optional short hyphenated title. The full filename should look like: `YYYY-MM-DD-E####-[optional-title].md`. Thus your very first notebook entry would look something like `2023-10-30-E0001-My-First-Entry.md`

You should commit and push your entries daily - preferably as you work and update them (you never know what might happen to your system).
Generically:
```console
$ git add _posts/*.md
$ git commit -m "useful message"
$ git push
```



## Chirpy Theme Features
We're using Chirpy - while you shouldn't need it, you can check out [documentation](https://github.com/cotes2020/jekyll-theme-chirpy#documentation) and license details below.

[![Gem Version](https://img.shields.io/gem/v/jekyll-theme-chirpy)][gem]&nbsp;
[![GitHub license](https://img.shields.io/github/license/cotes2020/chirpy-starter.svg?color=blue)][mit]

### License

Chirpy is published under [MIT][mit] License.

[gem]: https://rubygems.org/gems/jekyll-theme-chirpy
[chirpy]: https://github.com/cotes2020/jekyll-theme-chirpy/
[use-template]: https://github.com/cotes2020/chirpy-starter/generate
[CD]: https://en.wikipedia.org/wiki/Continuous_deployment
[mit]: https://github.com/cotes2020/chirpy-starter/blob/master/LICENSE

You lab notebook is  <span xmlns:cc="http://creativecommons.org/ns#" > licensed under <a href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></span> 